import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityWeatherInfoComponent } from './app/city-weather-info/city-weather-info.component';
import { CitySearchComponent } from './app/city-search/city-search.component';

const routes: Routes = [
  { path: '' ,redirectTo: 'city-weather/search', pathMatch: 'full' },
  { path: 'city-weather/search', component: CitySearchComponent },
  { path: 'city-weather/info',      component: CityWeatherInfoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
