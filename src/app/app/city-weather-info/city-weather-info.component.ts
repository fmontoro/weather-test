import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild
} from "@angular/core";
import { Router } from "@angular/router";
import { WeatherInfoService } from "../../service/weather-info.service";

@Component({
  selector: "app-city-weather-info",
  templateUrl: "./city-weather-info.component.html",
  styleUrls: ["./city-weather-info.component.scss"]
})
export class CityWeatherInfoComponent implements OnInit {
  //toggle button element reference
  @ViewChild("scaleInput") scaleInput: ElementRef;

  date: string = "";
  cityName: string = "";
  //Current Description of Weather - Current Weather Section
  currentWeatherDescription: string = "";
  //Current Weather Icon - Current Weather Section
  currentWeatherIcon: string = "";
  //Unit used in current temperature - Current Weather Section
  scale: string = "C";
  //Url used to save in local storage
  url: string = "";
  //Current Weather Temperature - Current Weather Section
  currentWeatherTemperature: number = 0;
  //Current Weather Time Array
  currentWeatherTime: any[] = [];
  //Current Weather Temmperature List -
  currentTimeTempList = [];
  //Daily Weather List Array - Footer Section
  weekWeatherList = [];
  weekWeatherCurrentDay: any[] = [];

  constructor(
    public weatherService: WeatherInfoService,
    private router: Router
  ) {}

  /**
   * Gets refresh event. If user refreshs
   * browser the url that was used to make the request
   * will be saved in local storage
   */
  @HostListener("window:beforeunload", ["$event"])
  refreshEvent($event) {
    $event.returnValue = "Refresh will save city and make another api call";
    localStorage.setItem("url", this.url);
    return false;
  }

  /**
   * Checks url key in local storage. If exists, will be used
   * to make a new api call.
   */
  ngOnInit() {
    this.url = this.weatherService.url;

    if (localStorage.getItem("url") !== null) {
      this.weatherService.url = localStorage.getItem("url");
      this.weatherService.search().subscribe(
        data => {
          this.weatherService.info = data;
          this.initialize();
        },
        error => {
          this.router.navigateByUrl("/city-weather/search");
        }
      );
    } else {
      this.initialize();
    }
  }

  /**
   * Back button arrow event. returns to homepage
   */
  backHome(event) {
    this.router.navigateByUrl("/city-weather/search");
  }

  /**
   * Initialize all values needed to show in the view
   */
  initialize() {
    this.scaleInput.nativeElement.checked = true;

    //If no data is set then it returns to homepage.
    if (this.weatherService.info == undefined) {
      this.router.navigateByUrl("/city-weather/search");
    }
    this.cityName = this.weatherService.info.city.name;
    this.currentWeather();
    this.weeklyWeather();
  }

  /**
   * Formats date as needed (Day as text, Month as Text, day as ordinal number and year)
   */
  formatDate(date) {
    let dateNewFormat = new Date(date)
      .toLocaleDateString("en-US", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric"
      })
      .split(",")
      .join(" ");
    let dateSplit = dateNewFormat.split(" ");
    let fourthArgument = "";
    switch (dateSplit[3]) {
      case "1": {
        fourthArgument = dateSplit[3] + "st";
        break;
      }
      case "2": {
        fourthArgument = dateSplit[3] + "nd";
        break;
      }
      case "3": {
        fourthArgument = dateSplit[3] + "rd";
        break;
      }
      default: {
        fourthArgument = dateSplit[3] + "th";
        break;
      }
    }

    return (
      dateSplit[0] +
      ", " +
      dateSplit[2] +
      " " +
      fourthArgument +
      " " +
      dateSplit[5]
    );
  }

  /**
   * Gets time of day for whole day weather list
   *
   */
  checkInclude(include: string) {
    switch (include) {
      case "03": {
        return "Morning";
      }
      case "09": {
        return "Day";
      }
      case "15": {
        return "Evening";
      }
      case "21": {
        return "Night";
      }
    }
  }

  /**
   * Set current weather values and data
   * Current Weather is set with first element of array returned from the api
   * It is possible to make another api call to the weahter endpoint, yet it is not implemented.
   */
  currentWeather() {
    this.currentWeatherTemperature = this.weatherService.info.list[0].main.temp;
    this.currentWeatherDescription = this.weatherService.info.list[0].weather[0].description
      .split(" ")
      .map(word => {
        return word.charAt(0).toUpperCase() + word.substr(1);
      })
      .join(" ");

    let currentTime = this.weatherService.info.list[0].dt_txt.split(" ")[0];

    this.date = this.formatDate(this.weatherService.info.list[0].dt_txt);
    let counter = 0;

    //this.currentTimeTempList = [];
    for (let i = 0; i < this.weatherService.info.list.reverse().length; i++) {
      /**
       * Checks what times are left of current day in the array.
       * Times that comes in the response are 00,03,09,12,15,18,21
       * one weather data every three hours.
       * Since list is reversed it will start from the last.
       * Api removes times depending on your current location time
       * if your time is 15 o clock it will remove 00,03,09 and 12 hours.
       */

      if (
        this.weatherService.info.list[i].dt_txt
          .split(" ")[0]
          .includes(currentTime) &&
        i % 2 === 0
      ) {
        let timeOfDay = this.checkInclude(
          this.weatherService.info.list[i].dt_txt.split(" ")[1].split(":")[0]
        );

        this.currentTimeTempList.push({
          daytime: timeOfDay,
          temp: this.weatherService.info.list[i].main.temp
        });
      }
    }

    this.currentTimeTempList = this.currentTimeTempList.reverse();
  }

  /**
   * Changes all temperatures values.
   */
  scaleChange() {
    let weekWeatherListTemp = this.weekWeatherList;
    let currentTimeTempListTemp = this.currentTimeTempList;
    this.currentTimeTempList = [];
    this.weekWeatherList = [];

    weekWeatherListTemp.forEach((item, index) => {
      weekWeatherListTemp[index].temp = this.scaleCalculation(
        this.scaleInput.nativeElement.checked,
        weekWeatherListTemp[index].temp
      );
    });

    currentTimeTempListTemp.forEach((item, index) => {
      currentTimeTempListTemp[index].temp = this.scaleCalculation(
        this.scaleInput.nativeElement.checked,
        currentTimeTempListTemp[index].temp
      );
    });

    this.currentWeatherTemperature = this.scaleCalculation(
      this.scaleInput.nativeElement.checked,
      this.currentWeatherTemperature
    );
    this.weekWeatherList = weekWeatherListTemp;
    this.currentTimeTempList = currentTimeTempListTemp;
  }

  /**
   * Changes current scale value from celsius to fahrenheit
   * or from Fahrenheit to celsius
   */
  scaleCalculation(checked: boolean, value: number): number {
    if (checked == true) {
      //to celsius
      this.scale = "C";
      return ((value - 32) * 5) / 9;
    } else {
      //to faranheit
      this.scale = "F";
      return 1.8 * value + 32;
    }
  }

  
  
  /**
   * Get the icon class to be used in the footer or current weather sections
   * weatherStatus value  is set from api response
   */
  getIcons(weatherStatus: number, daytime: boolean) {
    //openweatherapi weather conditions
    //https://openweathermap.org/weather-conditions
    //2xx thunderstorm
    //3xx Drizzle
    //5xx Rain
    //6xx Snow
    //7xx (Atmosphere)
    //800 Clear
    //80x Clouds
    let iconApi = "wi wi-";
    let iconDay = "day-";
    let iconNight = "night-alt-";
    let iconPathDay = `${iconApi}${iconDay}`;
    let iconPathNight = `${iconApi}${iconNight}`;
    let icon = "";
    
    if (200 >= weatherStatus && weatherStatus <= 300) {
      //thundestorm
      icon =
      daytime
          ? `${iconPathDay}thunderstorm`
          : `${iconPathNight}thunderstorm`;
    } else if (300 >= weatherStatus && weatherStatus <= 400) {
      //Drizzle

      icon = daytime ? `${iconPathDay}hail` : `${iconPathNight}hail`;
    } else if (500 >= weatherStatus && weatherStatus <= 600) {
      //Rain
      icon = daytime ? `${iconPathDay}rain` : `${iconPathNight}rain`;
    } else if (600 >= weatherStatus && weatherStatus <= 700) {
      //Snow
      icon = daytime ? `${iconPathDay}snow` : `${iconPathNight}snow`;
    } else if (700 >= weatherStatus && weatherStatus <= 800) {
      //Atmosphere
    } else if (weatherStatus == 800) {
      //Clear
      icon = daytime? `${iconPathDay}sunny` : `${iconApi}night-clear`;
    } else if (weatherStatus > 800) {
      icon =
        daytime ? `${iconPathDay}cloudy` : `${iconPathNight}cloudy`;
    } else {
      console.log(`icon not found`);
    }

    return icon;
  }

  /**
   * Sets weather for week
   */
  weeklyWeather() {
    let reverse = this.weatherService.info.list;
    let currentTime = reverse[0].dt_txt.split(" ")[1];
    for (let weekWeatherItem of reverse) {
      //checks for all days that have the same time as the current day
      if (weekWeatherItem.dt_txt.includes(currentTime)) {
        //checks for day or night
        let daytime =
          parseInt(weekWeatherItem.dt_txt.split(" ")[1].split(":")[0]) <= 15;

        let item = {
          day: new Date(weekWeatherItem.dt_txt).toLocaleDateString("en-us", {
            weekday: "long"
          }),
          temp: weekWeatherItem.main.temp,
          icon: this.getIcons(weekWeatherItem.weather[0].id, daytime)
        };
        this.weekWeatherList.push(item);
      }
    }
    this.currentWeatherIcon = this.weekWeatherList[0].icon;
  }
}
