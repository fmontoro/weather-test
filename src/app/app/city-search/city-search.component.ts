import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { WeatherInfoService } from "../../service/weather-info.service";

@Component({
  selector: "app-city-search",
  templateUrl: "./city-search.component.html",
  styleUrls: ["./city-search.component.scss"]
})
export class CitySearchComponent implements OnInit {
  cityInput: string = "";
  //variable biding to handle errors with the view
  error: string = "";

  constructor(
    private router: Router,
    public http: HttpClient,
    public weatherService: WeatherInfoService
  ) {}

  ngOnInit() {
    /**
     * Checks if there is any url key in local storage
     * and then it is removed. This key is used for the refreshed event.
     * Since the app starts here, it needs to be removed.
     */
    if (localStorage.getItem("url") !== null) {
      localStorage.removeItem("url");
    }
  }

  /**
   * Api Search with params.
   */
  search() {
    this.weatherService.setParams();
    this.weatherService.search().subscribe(
      data => {
        this.weatherService.info = data;
        this.router.navigateByUrl("/city-weather/info");
      },
      error => {
        this.cityInput = "";
        this.error = error.error.message;
      }
    );
  }

  /**
   * Sets params with input
   */
  inputSearch(city) {
    this.weatherService.data = { city: city.value };
    this.weatherService.checker = true;
    this.search();
  }

  /**
   * Sets params with navigator geolocation
   */
  geolocationSearch(event) {
    if (window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        data => {
          this.weatherService.data = {
            latitude: data.coords.latitude,
            longitude: data.coords.longitude
          };
          this.weatherService.checker = false;
          this.search();
        },
        error => {
          this.error = "Enable Geolocation";
        }
      );
    } else {
      this.error = "Geolocation is not supported by this browser.";
    }
  }
}
