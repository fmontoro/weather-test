import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class WeatherInfoService {
  //openweather api key
  API_KEY: string = "edf2c186d24fc54f41831bd2b784f257";
  //endpoint wihtout params
  api: string = "http://api.openweathermap.org/data/2.5/forecast?";
  //checks for input or geolocation search
  checker: boolean = true;
  //input data or geolocation coordinates
  data = {};
  //Api search result
  info: any;
  //url to request with params
  url: string = "";

  constructor(public http: HttpClient) {}

  /**
   * Sets params of url to request
   */
  setParams() {
    //let params='';
    let params = this.checker
      ? `q=${this.data["city"]}`
      : `lat=${this.data["latitude"]}&lon=${this.data["longitude"]}`;
    // if(this.checker){
    //   params=`q=${this.data['city']}`
    // }else{
    //   params=`lat=${this.data['latitude']}&lon=${this.data['longitude']}`
    // }
    this.url = `${this.api}${params}&APPID=${this.API_KEY}&units=metric`;
  }

  /**
   * Returns observable of request.
   */
  search() {
    return this.http.get(this.url);
  }
}
