import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CitySearchComponent } from './app/city-search/city-search.component';
import { CityWeatherInfoComponent } from './app/city-weather-info/city-weather-info.component';

@NgModule({
  declarations: [
    AppComponent,
    CitySearchComponent,
    CityWeatherInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
